from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.event_firing_webdriver import EventFiringWebElement
from selenium.webdriver.remote.webelement import WebElement
from selenium.common import exceptions
from bs4 import BeautifulSoup as soup
from connectionq import ConnectionQ
from handlerq import HandlerQ


class QUANT(ConnectionQ, HandlerQ):
    def __init__(self):
        super().__init__()
        self.selenium_url = f'https://selenium-python.readthedocs.io'
        self.tsetmc = f'http://tsetmc.com/Loader.aspx?ParTree=15'
        self.symbols_url = f"http://www.tsetmc.com/Loader.aspx?ParTree=111C1417"
        self.compare_url = f"http://tsetmc.com/Loader.aspx?ParTree=15131X"
        self.data_url = f"http://www.tsetmc.com/Loader.aspx?ParTree=151311"
        self.driver = ConnectionQ.driver(self)
        self.handler = HandlerQ()


    def gosel(self):
        self.driver.get(self.tsetmc)
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.ID, 'TopBar'))
            )
            if element:
                self.driver.close()
        except:
            self.driver.close()

    def getsymbols(self):
        data = {'symbol': [], 'url': []}
        self.driver.get(self.symbols_url)
        element = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, '//table[@id="tblToGrid"]'))
        )
        if element:
            flag = True
            row = 2
            while flag:
                try:
                    if str(self.driver.find_element_by_xpath(f'//table[@id="tblToGrid"]/tbody/tr[{row}]/td[7]/a').text)[-1] != 'ح':
                        data['symbol'].append(self.driver.find_element_by_xpath(f'//table[@id="tblToGrid"]/tbody/tr[{row}]/td[7]').text)
                        data['url'].append(self.driver.find_element_by_xpath(f'//table[@id="tblToGrid"]/tbody/tr[{row}]/td[7]/a').get_attribute('href'))
                    if self.driver.find_element_by_xpath(f'//table[@id="tblToGrid"]/tbody/tr[{row}]/td[7]/a').text == 'ونيكي':
                        break
                    else :
                        row += 1
                except:
                    break
            self.driver.close()
            return data

    def getcompare(self):
        data = {"stocks": [], "values": []}
        result = self.db.symbols.find({}).limit(5)
        self.driver.get(self.compare_url)
        element = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, "//div[@id='ModalWindowOuter1']/div[2]")))
        if element:
            element.click()
            try:
                for j, r in enumerate(result):
                    print(f"ready to fetch {r['symbol']} records\n")
                    self.driver.find_element_by_xpath("//tr[@id='Header']/th/a").click()
                    try:
                        element = WebDriverWait(self.driver, 5).until(
                            EC.presence_of_element_located((By.XPATH, "//input[@id='SearchKey']")))
                        if element:
                            element.send_keys(f"{r['symbol']}")
                        else:
                            print(f"element is the missing part")
                            continue
                        try:
                            elem = WebDriverWait(self.driver, 5).until(
                                EC.presence_of_element_located(
                                    (By.XPATH, "//div[@id='SearchResult']/div/div[2]/table/tbody/tr/td/a")))
                            if elem:
                                elem.click()
                            try:
                                ele = WebDriverWait(self.driver, 5).until(
                                    EC.presence_of_element_located(
                                        (By.XPATH, "//div[contains(@class, 'popup_block ')]")))
                                if ele:
                                    self.driver.find_element_by_xpath("//div[contains(@class, 'popup_close')]").click()
                                    continue
                            except NoSuchElementException:
                                pass
                            except Exception as e:
                                # print(f"some odd error happened :: {e}")
                                pass
                        except Exception as e:
                            print(f"error :: {e}")
                        try:
                            v = []
                            html_source = self.driver.page_source
                            page_soup = soup(html_source, "html.parser")
                            for n, trs in enumerate(page_soup.find_all('tr')[3:]):
                                tds = trs.find_all('td')
                                if n not in [6, 13, 20, 26, 32, 35, 41, 44, 57, 66]:
                                    # v.append(tds[0].text if len(tds[0].text)>0 else '')
                                    element = self.driver.find_element_by_xpath(f'//td[@id="chart0"]/img')
                                    if element:
                                        element.screenshot(filename=f'{r["symbol"]}--plain.png')
                                    v.append(tds[1].text if len(tds[1].text) > 0 else '0')
                            data["stocks"].append(r['symbol'])
                            data["values"].append(v)
                            # print(v)
                            self.driver.find_element_by_xpath("//a[contains(text(),'x')]").click()
                            continue
                        except:
                            print(f"some e")

                    except Exception as e:
                        print(f"error is {e}")
            except Exception as e:
                print(f"supposedly mongo has some error :: {e}")
                return data
        return data

    def gethistory(self):

        data = {"stock": [], "pct_change": [], "close": [], "value": [], "volume": [], "transactions": [],
                "persian_date": [], "gregorian_date": [], "persian_year": [], "persian_month": [], "persian_day": [],
                "gregorian_year": [], "gregorian_month": [], "gregorian_day": [], "unixtime": []}
        stocks = self.db.symbols.find({}, {"symbol": 1}).sort("_id", -1).limit(1)

        self.driver.get(self.data_url)
        for j, r in enumerate(stocks):

            element = WebDriverWait(self.driver, 5).until(
                EC.presence_of_element_located((By.XPATH, "//a[@id='search']")))
            if element:
                element.click()
            element = WebDriverWait(self.driver, 5).until(
                EC.presence_of_element_located((By.XPATH, "//input[@id='SearchKey']")))
            if element:
                element.send_keys(f"{r['symbol']}")
                element = WebDriverWait(self.driver, 5).until(
                    EC.presence_of_element_located(
                        (By.XPATH, "//div[@id='SearchResult']/div/div[2]/table/tbody/tr/td/a")))
                if element:
                    element.click()
                element = WebDriverWait(self.driver, 5).until(
                    EC.presence_of_element_located((By.XPATH, "//a[contains(text(),'سابقه')]")))
                if element:
                    element.click()
                try:
                    element = WebDriverWait(self.driver, 10).until(
                        EC.presence_of_element_located((By.XPATH, "//div[@id='trade']/div[2]/table/tbody/tr[2]/td[16]"))
                    )
                    if element:
                        pagenumbers = self.driver.execute_script(
                            "return document.getElementsByClassName('pagingBlock')[0].childElementCount")
                        if pagenumbers > 1:
                            for j in range(1, pagenumbers):

                                element = self.driver.find_element_by_xpath(f"//div[@id='paging']/div/a[{j}]")
                                if element:
                                    element.click()
                                    flag = True
                                    row = 2
                                    while flag:
                                        try:
                                            for i in range(3, 17):
                                                data['stock'] = r['symbol']
                                                if i == 5:
                                                    data['pct_change'].append(float(self.driver.find_element_by_xpath(
                                                        f"//div[@id='trade']/div[2]/table/tbody/tr[{row}]/td[{i}]").text))
                                                elif i == 7:
                                                    data['close'].append(float(self.driver.find_element_by_xpath(
                                                        f"//div[@id='trade']/div[2]/table/tbody/tr[{row}]/td[{i}]").text.replace(
                                                        ',', '')))

                                                elif i == 13:
                                                    value = self.driver.find_element_by_xpath(
                                                        f"//div[@id='trade']/div[2]/table/tbody/tr[{row}]/td[{i}]").text.replace(
                                                        ',', '')
                                                    if value[-1:] == 'M':
                                                        value = float(value[:-1]) * 1000000
                                                    elif value[-1:] == 'B':
                                                        value = float(value[:-1]) * 1000000000
                                                    data['value'].append(value)
                                                elif i == 14:
                                                    volume = self.driver.find_element_by_xpath(
                                                        f"//div[@id='trade']/div[2]/table/tbody/tr[{row}]/td[{i}]").text.replace(
                                                        ',', '')
                                                    if volume[-1:] == 'M':
                                                        volume = float(volume[:-1]) * 1000000
                                                    elif volume[-1:] == 'B':
                                                        volume = float(volume[:-1]) * 1000000000
                                                    data['volume'].append(float(volume))
                                                elif i == 15:
                                                    transactions = self.driver.find_element_by_xpath(
                                                        f"//div[@id='trade']/div[2]/table/tbody/tr[{row}]/td[{i}]").text.replace(
                                                        ',', '')
                                                    data['transactions'].append(int(transactions))
                                                elif i == 16:
                                                    date = self.driver.find_element_by_xpath(
                                                        f"//div[@id='trade']/div[2]/table/tbody/tr[{row}]/td[16]").text
                                                    data['persian_date'].append(date)
                                                    # print(JalaliDate(int(date.split('/')[0]), int(date.split('/')[1]), int(date.split('/')[2])).to_gregorian().isoformat())
                                                # else:
                                                #     print(self.driver.find_element_by_xpath(f"//div[@id='trade']/div[2]/table/tbody/tr[{row}]/td[{i}]").text.replace(',', ''))
                                            row += 1
                                        except NoSuchElementException:
                                            flag = False
                                            break
                                        except:
                                            print(f"error occured on line {row} and td {i}")
                                            flag = False
                                            break
                        try:
                            if data['stock'] != []:
                                self.db.history.insert_one(data)
                                print(data)
                                data = {"stock": [], "pct_change": [], "close": [], "value": [], "volume": [],
                                        "transactions": [], "persian_date": [], "gregorian_date": [],
                                        "persian_year": [], "persian_month": [], "persian_day": [],
                                        "gregorian_year": [], "gregorian_month": [], "gregorian_day": [],
                                        "unixtime": []}
                        except:
                            pass

                except:
                    pass



if __name__ == '__main__':
    quantone = QUANT()
    quantone.gethistory()
    # symbols = quantone.getsymbols()
    # data = quantone.getcompare()

    # try:
    #     for idx, symbol in enumerate(symbols['symbol']):
    #         data = {
    #             'symbol': symbols['symbol'][idx],
    #             'url': symbols['url'][idx]
    #         }
    #         quantone.handler.db.symbols.insert_one(data)
    #         print(symbol)
    # except:
    #     print(symbol)
    #     pass
    # symbols = quantone.handler.db.symbols.find({})
    # for idx, symbol in enumerate(symbols):
    #     print(symbol['stock'])