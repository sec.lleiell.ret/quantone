from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.event_firing_webdriver import EventFiringWebElement
from selenium.webdriver.remote.webelement import WebElement
from selenium.common import exceptions
from connectionq import ConnectionQ
from handlerq import HandlerQ


class QUANT(ConnectionQ, HandlerQ):
    def __init__(self):
        super().__init__()
        self.selenium_url = f'https://selenium-python.readthedocs.io'
        self.tsetmc = f'http://tsetmc.com/Loader.aspx?ParTree=15'
        self.symbols_url = f"http://www.tsetmc.com/Loader.aspx?ParTree=111C1417"
        self.driver = ConnectionQ.driver(self)
        self.handler = HandlerQ()


    def gosel(self):
        self.driver.get(self.tsetmc)
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.ID, 'TopBar'))
            )
            if element:
                self.driver.close()
        except:
            self.driver.close()

    def getsymbols(self):
        data = {'symbol': [], 'url': []}
        self.driver.get(self.symbols_url)
        element = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, '//table[@id="tblToGrid"]'))
        )
        if element:
            flag = True
            row = 2
            while flag:
                try:
                    if str(self.driver.find_element_by_xpath(f'//table[@id="tblToGrid"]/tbody/tr[{row}]/td[7]/a').text)[-1] != 'ح':
                        data['symbol'].append(self.driver.find_element_by_xpath(f'//table[@id="tblToGrid"]/tbody/tr[{row}]/td[7]').text)
                        data['url'].append(self.driver.find_element_by_xpath(f'//table[@id="tblToGrid"]/tbody/tr[{row}]/td[7]/a').get_attribute('href'))
                    if self.driver.find_element_by_xpath(f'//table[@id="tblToGrid"]/tbody/tr[{row}]/td[7]/a').text == 'ونيكي':
                        break
                    else :
                        row += 1
                except:
                    break
            self.driver.close()
            return data



if __name__ == '__main__':
    quantone = QUANT()
    symbols = quantone.getsymbols()
    try:
        for idx, symbol in enumerate(symbols['symbol']):
            data = {
                'symbol': symbols['symbol'][idx],
                'url': symbols['url'][idx]
            }
            quantone.handler.db.symbols.insert_one(data)
            print(symbol)
    except:
        print(symbol)
        pass
    # symbols = quantone.handler.db.symbols.find({})
    # for idx, symbol in enumerate(symbols):
    #     print(symbol['stock'])