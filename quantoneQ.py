# firefox
# mongodb 4.4.5
# mongodbcompass
# python 3.8.3
# pycharm community edition 2020.3.5
# nodejs v14.17
# selenium https://selenium-python.readthedocs.io
# xpath https://devhints.io/xpath
# selenium ide
# selenium exceptions
# mongodb://localhost:27017


from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from connectionq import ConnectionQ
from handlerq import HandlerQ

class QUANT(ConnectionQ, HandlerQ):
    def __init__(self):
        super().__init__()
        self.selenium_url = f'https://selenium-python.readthedocs.io'
        self.tsetmc = f'http://tsetmc.com/Loader.aspx?ParTree=15'
        self.driver = ConnectionQ.driver(self)
        self.handler = HandlerQ()


    def gosel(self):
        self.driver.get(self.tsetmc)
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.ID, 'TopBar'))
            )
            if element:
                self.driver.close()
        except:
            self.driver.close()


if __name__ == '__main__':
    quantone = QUANT()
    quantone.gosel()


# connectionq.py

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.event_firing_webdriver import EventFiringWebElement
from selenium.webdriver.remote.webelement import WebElement
from selenium.common import exceptions




class ConnectionQ:

    def driver(self):
        options = webdriver.FirefoxOptions()
        #   optıons
        #   options
        #   options
        self.driver = webdriver.Firefox(options=options)

        return self.driver



# handlerq.py


from pymongo import MongoClient



class HandlerQ:
    def __init__(self):
        self.client = MongoClient(port=27017)
        self.db = self.client.quantonedb

